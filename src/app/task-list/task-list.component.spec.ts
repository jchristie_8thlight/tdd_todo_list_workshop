import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskListComponent, Task } from './task-list.component';
import { By } from '@angular/platform-browser';

describe('TaskListComponent', () => {
  let component: TaskListComponent;
  let fixture: ComponentFixture<TaskListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it(`complains that it's lonely and needs tasks when empty`, () => {
    const lonelyMessage = fixture.debugElement
      .query(By.css('span#lonely-message'));

    expect(lonelyMessage).not.toBeNull();
  });

  it('displays a given list of tasks', () => {
    const cleaningTask: Task = { description: 'Clean the house' };
    const laundryTask: Task = { description: 'Do the laundry' };

    const tasks: Task[] = [cleaningTask, laundryTask];

    component.tasks = tasks;

    fixture.detectChanges();

    const taskList = fixture.debugElement
      .query(By.css('div.task-list'));

    expect(taskList).not.toBeNull();
    expect(taskList.children.length).toEqual(2);

    const domCleaningTask = taskList.query(By.css('div#task-0'));
    const domLaundryTask = taskList.query(By.css('div#task-1'));

    expect(domCleaningTask).not.toBeNull();
    expect(domCleaningTask.nativeElement.innerHTML)
      .toContain(cleaningTask.description);

    expect(domLaundryTask).not.toBeNull();
    expect(domLaundryTask.nativeElement.innerHTML)
      .toContain(laundryTask.description);
  });

  it('does not display the lonely message when tasks are given', () => {
    const exampleTask: Task = { description: 'An example task' };
    component.tasks = [exampleTask];

    fixture.detectChanges();

    const lonelyMessage = fixture.nativeElement
      .querySelectorAll('span#lonely-message');

    expect(lonelyMessage.length).toEqual(0);
  });
});
