import { By } from '@angular/platform-browser';
import { TestBed, async } from '@angular/core/testing';
import { ComponentFixture } from '@angular/core/testing';

import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('renders a list of tasks', () => {
    const taskList = fixture.debugElement
      .query(By.css('app-task-list'));

    expect(taskList).not.toBeNull();
  });

  it('renders a set of controls for the list', () => {
    const listControls = fixture.debugElement
      .query(By.css('app-list-controls'));

    expect(listControls).not.toBeNull();
  });
});
